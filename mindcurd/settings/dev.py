from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k0a153wxwpies&6y+2)c(5b90rgf4%svgtvn31)8fl!h9@n-_5'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['.mindcurd.com', '*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


try:
    from .local import *
except ImportError:
    pass
