import requests
from bs4 import BeautifulSoup
import re


url='http://www.nationmaster.com/country-info/stats/Religion/Religions/All'

response = requests.get(url)

soup = BeautifulSoup(response.content)

soup.find_all('tbody')[0].find_all('tr')[0].find_all('td')[1].text.strip()

religion_list_table= soup.find_all('tbody')[0]

rows=religion_list_table.find_all('tr')

country_religion_details={}

class ReligionRecord():
    name=''
    percentage=0.0

    def __init__(self, name , percentage):
        self.name=name
        self.percentage=percentage

    def __str__(self):
        return '{}:{}'.format(self.name, str(self.percentage))

religion_name_pattern=r'[a-z\-\'\(\)A-Z\s]*'

rel_det= lambda x : (re.search(religion_name_pattern,x.strip()).group(0), re.split(religion_name_pattern,x.strip())[1][:-1])

for row in rows :
    columns=row.find_all('td')
    if columns is not None and len(columns)==2:
        country=columns[0].text.strip()
        religions_details=columns[1].text.split(',')
        print('{} {}'.format(country, religions_details))
        religions=[]
        for detail in religions_details:
            det=rel_det(detail)
            rec=ReligionRecord(det[0],det[1])
            religions.append(rec)

        country_religion_details[country]=religions


    else:
        print('no column found')

print('all done')

