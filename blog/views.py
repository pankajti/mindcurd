from django.shortcuts import render

# Create your views here.

def serve_research(request):
    path =request.path
    return render(request, 'research{}'.format(path.split('/research')[1]),{})

